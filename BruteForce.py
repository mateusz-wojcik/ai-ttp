from random import shuffle, sample, random, randint, uniform
import csv
from Individual import TTPIndividual
import numpy as np
import itertools


class BruteForce:
    def __init__(self, ttp, cities_list, iterations, csv_file=None, logging_interval=None, csv_mode='w', pop_size=None):
        self.ttp = ttp
        self.population = []
        self.csv_File = csv_file
        self.logging_interval = logging_interval
        self.csv_mode = csv_mode
        self.cities_list = cities_list
        self.iterations = iterations
        self.bests = []
        self.averages = []
        self.weakest = []
        self.pop_size = pop_size

    def initialize_population(self):
        for i in range(self.pop_size):
            individual = TTPIndividual(self.ttp.shortest_route_tsp(), self.ttp.init_knapsack())
            self.population.append(individual)

    def main(self):
        best_solution = None
        #best_solution = self.main_logging_csv()
        best_solution = self.main_greedy()
        return best_solution

    def main_greedy(self):
        self.bests = []
        self.averages = []
        self.weakest = []
        with open('csv_files/' + self.csv_File, self.csv_mode) as file:
            writer = csv.writer(file)
            writer.writerow(['gen', 'best', 'average', 'weakest'])

            t = 0
            self.initialize_population()
            for i in range(self.iterations):

                if t % 1000 == 0:
                    print(t)

                if t % self.logging_interval == 0:
                    tpl = self.evaluate_feedback_random()
                    self.bests.append(np.round(tpl[0], decimals=2))
                    self.averages.append(np.round(tpl[1], decimals=2))
                    self.weakest.append(np.round(tpl[2], decimals=2))
                    writer.writerow([str(t), str(np.round(tpl[0], decimals=2)), str(np.round(tpl[1], decimals=2)),
                                     str(np.round(tpl[2], decimals=2))])
                else:
                    self.evaluate_random()
                t = t + 1

                if t == self.iterations:
                    break

            best_solution = self.select_best_fitness_one(self.population)
        return best_solution

    def main_logging_csv(self):
        self.bests = []
        self.averages = []
        self.weakest = []
        with open('csv_files/' + self.csv_File, self.csv_mode) as file:
            writer = csv.writer(file)
            writer.writerow(['gen', 'best', 'average', 'weakest'])

            t = 0

            perms = list(itertools.permutations(self.cities_list))
            print(len(perms))

            for item in perms:
                permutation = list(item)
                self.population.append(TTPIndividual(permutation, self.ttp.init_knapsack()))

                if t % 1000 == 0:
                    print(t)

                if t % self.logging_interval == 0:
                    tpl = self.evaluate_feedback()
                    self.bests.append(np.round(tpl[0], decimals=2))
                    self.averages.append(np.round(tpl[1], decimals=2))
                    self.weakest.append(np.round(tpl[2], decimals=2))
                    writer.writerow([str(t), str(np.round(tpl[0], decimals=2)), str(np.round(tpl[1], decimals=2)),
                                     str(np.round(tpl[2], decimals=2))])
                else:
                    self.evaluate()
                t = t + 1

                if t == self.iterations:
                    break

            best_solution = self.select_best_fitness_one(self.population)
        return best_solution

    def evaluate(self):
        for i in range(len(self.population)):
            if self.population[i].fitness is None:
                self.population[i].fitness = self.ttp.g_x_y(self.population[i].route, self.population[i].knapsack)

    def evaluate_feedback(self):
        best, average_fitness, weakest = self.population[0], .0, self.population[0]
        for i in range(len(self.population)):
            if self.population[i].fitness is None:
                self.population[i].fitness = self.ttp.g_x_y(self.population[i].route, self.population[i].knapsack)
            if self.population[i].fitness > best.fitness:
                best = self.population[i]
            elif self.population[i].fitness < weakest.fitness:
                weakest = self.population[i]
            average_fitness += self.population[i].fitness
        return best.fitness, average_fitness / len(self.population), weakest.fitness

    def evaluate_random(self):
        for i in range(len(self.population)):
            if self.population[i].fitness is None:
                self.population[i].fitness = self.ttp.g_x_y_random(self.population[i].route, self.population[i].knapsack)

    def evaluate_feedback_random(self):
        best, average_fitness, weakest = self.population[0], .0, self.population[0]
        for i in range(len(self.population)):
            if self.population[i].fitness is None:
                self.population[i].fitness = self.ttp.g_x_y_random(self.population[i].route, self.population[i].knapsack)
            if self.population[i].fitness > best.fitness:
                best = self.population[i]
            elif self.population[i].fitness < weakest.fitness:
                weakest = self.population[i]
            average_fitness += self.population[i].fitness
        return best.fitness, average_fitness / len(self.population), weakest.fitness

    @staticmethod
    def select_best_fitness_one(pop):
        return max(pop, key=lambda p: p.fitness)

    def print_population(self):
        for one in self.population:
            print(one.fitness, one.route, one.knapsack)

    def print_population_fragment(self, fragment):
        print('population fragment')
        for one in fragment:
            print(one.fitness, one.route, one.knapsack)
