import time
import numpy as np
from GeneticAlgorithm import GeneticAlgorithm
from Loader import Loader
from TTP import TTP
import itertools

# POP_SIZE = 100
# GEN = 100
# PX = 0.7
# PM = 0.01
# TOUR = 5

LOG_INTERVAL = 1


def run_multiple_times(file_name, times, tested_parameter, **parameters):
    print("start " + tested_parameter, time.asctime(time.localtime(time.time())))
    loader = Loader(file_name + '.ttp')
    loader.load()
    loader.parse()
    bests = np.asarray([])
    averages = np.asarray([])
    weakest = np.asarray([])
    for i in range(times):
        b, a, w = run_simulation(file_name, loader, 'w' if i == 0 else 'a', True if i == 0 else False, True, i,
                                 **parameters)

        if i == 0:
            bests = np.append(bests, np.asarray(b))
            averages = np.append(averages, np.asarray(a))
            weakest = np.append(weakest, np.asarray(w))
        else:
            bests = np.column_stack((bests, np.asarray(b)))
            averages = np.column_stack((averages, np.asarray(a)))
            weakest = np.column_stack((weakest, np.asarray(w)))

    bests_av = bests.mean(axis=1)
    averages_av = averages.mean(axis=1)
    weakest_av = weakest.mean(axis=1)
    best_std = np.std(bests, axis=1)
    averages_std = np.std(averages, axis=1)
    weakest_std = np.std(weakest, axis=1)
    summary = ['średnia_best', bests_av.mean(), 'std_best', np.std(bests_av),
               'średnia_a', averages_av.mean(), 'std_av', np.std(averages_av),
               'średnia_w', weakest_av.mean(), 'std_w', np.std(weakest_av)]
    TTP.save_list_to_file(
        file_name + '_' + tested_parameter + '_' + str(parameters[tested_parameter]) + '_' + 'best_std.txt', best_std)
    TTP.save_list_to_file(
        file_name + '_' + tested_parameter + '_' + str(parameters[tested_parameter]) + '_' + 'averages_std.txt', averages_std)
    TTP.save_list_to_file(
        file_name + '_' + tested_parameter + '_' + str(parameters[tested_parameter]) + '_' + 'weakest_std.txt', weakest_std)
    TTP.save_list_to_file(
        file_name + '_' + tested_parameter + '_' + str(parameters[tested_parameter]) + '_' + 'summary.txt',
        summary)
    # print('best_std', best_std)
    # print('averages_std', averages_std)
    # print('weakest_std', weakest_std)
    TTP.visualize_population(bests_av, averages_av, weakest_av,
                             file_name + '_' + tested_parameter + '_' + str(parameters[tested_parameter]))


def run_simulation(file_name, loader, csv_mode, visualize=False, visualize_pop=False, iteration=None, **parameters):
    ttp = TTP(loader.parameters)

    ga = GeneticAlgorithm(parameters['POP_SIZE'], parameters['GEN'], parameters['PX'], parameters['PM'],
                          parameters['TOUR'], ttp,
                          file_name + '.csv', LOG_INTERVAL, csv_mode)
    best_solution = ga.main()
    # print(best_solution.route)
    # print(best_solution.knapsack.items)
    # print("weight", best_solution.knapsack.get_weight(), "max_weight", best_solution.knapsack.max_weight)
    # print("profit", best_solution.knapsack.get_value(), "profit", best_solution.knapsack.value)
    # print("best fitness", best_solution.fitness)
    print("finish ", time.asctime(time.localtime(time.time())))
    # print(best_solution.fitness)
    if visualize:
        ttp.visualize(best_solution.route, file_name)

    return ga.bests, ga.averages, ga.weakest


run_multiple_times('trivial_0', 10, tested_parameter='TOUR', POP_SIZE=100, GEN=100, PX=0.7, PM=0.01, TOUR=5)


# # tournament
#
#
# run_multiple_times('medium_3', 10, tested_parameter='TOUR', POP_SIZE=100, GEN=100, PX=0.7, PM=0.01, TOUR=0)
# run_multiple_times('medium_3', 10, tested_parameter='TOUR', POP_SIZE=100, GEN=100, PX=0.7, PM=0.01, TOUR=1)
# run_multiple_times('medium_3', 10, tested_parameter='TOUR', POP_SIZE=100, GEN=100, PX=0.7, PM=0.01, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='TOUR', POP_SIZE=100, GEN=100, PX=0.7, PM=0.01, TOUR=10)
# run_multiple_times('medium_3', 10, tested_parameter='TOUR', POP_SIZE=100, GEN=100, PX=0.7, PM=0.01, TOUR=25)
# run_multiple_times('medium_3', 10, tested_parameter='TOUR', POP_SIZE=100, GEN=100, PX=0.7, PM=0.01, TOUR=100)
#
# # px
#
#
# run_multiple_times('medium_3', 10, tested_parameter='PX', POP_SIZE=100, GEN=100, PX=0.0, PM=0.01, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='PX', POP_SIZE=100, GEN=100, PX=0.1, PM=0.01, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='PX', POP_SIZE=100, GEN=100, PX=0.2, PM=0.01, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='PX', POP_SIZE=100, GEN=100, PX=0.4, PM=0.01, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='PX', POP_SIZE=100, GEN=100, PX=0.6, PM=0.01, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='PX', POP_SIZE=100, GEN=100, PX=0.8, PM=0.01, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='PX', POP_SIZE=100, GEN=100, PX=1.0, PM=0.01, TOUR=5)
#
# # pm
#
#
# run_multiple_times('medium_3', 10, tested_parameter='PM', POP_SIZE=100, GEN=100, PM=0.0, PX=0.7, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='PM', POP_SIZE=100, GEN=100, PM=0.005, PX=0.7, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='PM', POP_SIZE=100, GEN=100, PM=0.01, PX=0.7, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='PM', POP_SIZE=100, GEN=100, PM=0.05, PX=0.7, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='PM', POP_SIZE=100, GEN=100, PM=0.1, PX=0.7, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='PM', POP_SIZE=100, GEN=100, PM=0.2, PX=0.7, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='PM', POP_SIZE=100, GEN=100, PM=0.5, PX=0.7, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='PM', POP_SIZE=100, GEN=100, PM=1.0, PX=0.7, TOUR=5)
#
# # gen
#
#
# run_multiple_times('medium_3', 10, tested_parameter='GEN', POP_SIZE=100, GEN=20, PX=0.7, PM=0.01, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='GEN', POP_SIZE=100, GEN=50, PX=0.7, PM=0.01, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='GEN', POP_SIZE=100, GEN=100, PX=0.7, PM=0.01, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='GEN', POP_SIZE=100, GEN=500, PX=0.7, PM=0.01, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='GEN', POP_SIZE=100, GEN=1000, PX=0.7, PM=0.01, TOUR=5)
#
# # pop_size
#
#
# run_multiple_times('medium_3', 10, tested_parameter='POP_SIZE', POP_SIZE=20, GEN=100, PX=0.7, PM=0.01, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='POP_SIZE', POP_SIZE=50, GEN=100, PX=0.7, PM=0.01, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='POP_SIZE', POP_SIZE=100, GEN=100, PX=0.7, PM=0.01, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='POP_SIZE', POP_SIZE=300, GEN=100, PX=0.7, PM=0.01, TOUR=5)
# run_multiple_times('medium_3', 10, tested_parameter='POP_SIZE', POP_SIZE=1000, GEN=100, PX=0.7, PM=0.01, TOUR=5)
