from random import shuffle, sample, random, randint, uniform
import csv
from Individual import TTPIndividual
import numpy as np


class GeneticAlgorithm:
    def __init__(self, pop_size, gen, px, pm, tour, ttp, csv_file=None, logging_interval=None, csv_mode='w'):
        self.pop_size = pop_size
        self.gen = gen
        self.px = px
        self.pm = pm
        self.tour = tour
        self.ttp = ttp
        self.population = []
        self.csv_File = csv_file
        self.logging_interval = logging_interval
        self.csv_mode = csv_mode
        self.bests = []
        self.averages = []
        self.weakest = []

    def main(self):
        best_solution = None
        if self.csv_File is None:
            best_solution = self.main_normal()
        else:
            best_solution = self.main_logging_csv()
        return best_solution

    def main_logging_csv(self):
        self.bests = []
        self.averages = []
        self.weakest = []
        with open('csv_files/' + self.csv_File, self.csv_mode) as file:
            writer = csv.writer(file)
            writer.writerow(['gen', 'best', 'average', 'weakest'])

            t = 0
            self.initialize_population()
            self.evaluate()

            while t <= self.gen:
                self.selection_tournament_only_winners()
                #self.selection_roulette()
                self.crossover('proper')
                self.mutation_gene()
                if t % self.logging_interval == 0:
                    tpl = self.evaluate_feedback()
                    self.bests.append(np.round(tpl[0], decimals=2))
                    self.averages.append(np.round(tpl[1], decimals=2))
                    self.weakest.append(np.round(tpl[2], decimals=2))
                    writer.writerow([str(t), str(np.round(tpl[0], decimals=2)), str(np.round(tpl[1], decimals=2)),
                                     str(np.round(tpl[2], decimals=2))])
                else:
                    self.evaluate()
                t = t + 1

                # if t % 100 == 0 or t == 1 or t == 10:
                #     print("generation ", t)
                #     print("best fitness: ", self.select_best_fitness_one(self.population).fitness)

            best_solution = self.select_best_fitness_one(self.population)
        return best_solution

    def main_normal(self):
        t = 0
        self.initialize_population()
        self.evaluate()

        while t < self.gen:
            self.selection_tournament_only_winners()
            self.crossover('proper')
            self.mutation_gene()
            self.evaluate()
            t = t + 1

            if t % 100 == 0 or t == 1 or t == 10:
                print("generation ", t)
                print("best fitness: ", self.select_best_fitness_one(self.population).fitness)

        best_solution = self.select_best_fitness_one(self.population)
        return best_solution

    def initialize_population(self):
        for i in range(self.pop_size):
            individual = TTPIndividual(self.ttp.random_route_tsp(), self.ttp.init_knapsack())
            self.population.append(individual)

    def evaluate(self):
        for i in range(self.pop_size):
            if self.population[i].fitness is None:
                self.population[i].fitness = self.ttp.g_x_y(self.population[i].route, self.population[i].knapsack)

    def evaluate_feedback(self):
        best, average_fitness, weakest = self.population[0], .0, self.population[0]
        for i in range(self.pop_size):
            if self.population[i].fitness is None:
                self.population[i].fitness = self.ttp.g_x_y(self.population[i].route, self.population[i].knapsack)
            if self.population[i].fitness > best.fitness:
                best = self.population[i]
            elif self.population[i].fitness < weakest.fitness:
                weakest = self.population[i]
            average_fitness += self.population[i].fitness
        return best.fitness, average_fitness / self.pop_size, weakest.fitness

    @staticmethod
    def mutation_random_swap(route_tsp):
        index_1, index_2 = sample(range(len(route_tsp)), 2)
        route_tsp[index_1], route_tsp[index_2] = route_tsp[index_2], route_tsp[index_1]

    @staticmethod
    def mutation_inversion(route_tsp):
        index_1, index_2 = sample(range(len(route_tsp)), 2)
        min_, max_ = min(index_1, index_2), max(index_1, index_2)
        mutation_area = route_tsp[min_:max_]
        route_tsp[min_: max_] = reversed(mutation_area)

    @staticmethod
    def crossing_ox(parent_1, parent_2):
        def first_occurrence_index(item, list):
            return list.index(item)

        def fix(child, parent):
            rest = [i for i in parent if i not in child]
            for i in rest:
                child[first_occurrence_index(0, child)] = i

        size = len(parent_1)
        index_1, index_2 = sample(range(size), 2)
        min_, max_ = min(index_1, index_2), max(index_1, index_2)
        crossing_area_1 = parent_1[min_:max_]
        crossing_area_2 = parent_2[min_:max_]
        child_1, child_2 = [0] * size, [0] * size
        child_1[min_:max_] = crossing_area_1
        child_2[min_:max_] = crossing_area_2
        fix(child_1, parent_2)
        fix(child_2, parent_1)
        return child_1, child_2

    @staticmethod
    def chunk_list(l, n):
        return [l[i: i + n] for i in range(0, len(l), n)]

    @staticmethod
    def select_best_fitness_one(pop):
        return max(pop, key=lambda p: p.fitness)

    def crossing_properly(self):
        shuffle(self.population)
        for i in range(0, len(self.population), 2):
            if random() < self.px:
                parent_1, parent_2 = self.population[i], self.population[i + 1]
                child_1, child_2 = self.crossing_ox(parent_1.route, parent_2.route)
                self.population[i], self.population[i + 1] = TTPIndividual(child_1, self.ttp.init_knapsack()), \
                                                             TTPIndividual(child_2, self.ttp.init_knapsack())

    def fix_population_random_ones(self):
        shuffle(self.population)
        pop_len = len(self.population)
        new_individuals = []
        lack = self.pop_size - pop_len
        for _ in self.population:
            if random() < self.px:
                index_1, index_2 = sample(range(pop_len), 2)
                parent_1, parent_2 = self.population[index_1], self.population[index_2]
                child_1, child_2 = self.crossing_ox(parent_1.route, parent_2.route)
                if len(new_individuals) < lack:
                    new_individuals.append(TTPIndividual(child_1, self.ttp.init_knapsack()))
                    new_individuals.append(TTPIndividual(child_2, self.ttp.init_knapsack()))
        new_individuals = new_individuals[:lack]

        while len(new_individuals) < lack:
            new_individuals.append(TTPIndividual(self.ttp.random_route_tsp(), self.ttp.init_knapsack()))
        self.population.extend(new_individuals)

    def crossing_ox_all(self):
        shuffle(self.population)
        pop_len = len(self.population)
        i = 0
        while i < (pop_len - 1):
            if random() < self.px:
                parent_1, parent_2 = self.population[i], self.population[i + 1]
                child_1, child_2 = self.crossing_ox(parent_1.route, parent_2.route)
                self.population[i], self.population[i + 1] = TTPIndividual(child_1, self.ttp.init_knapsack()), \
                                                             TTPIndividual(child_2, self.ttp.init_knapsack())
                i = i + 1
            i = i + 1

    def fix_population_only_winners(self):
        shuffle(self.population)
        pop_len = len(self.population)
        lack = self.pop_size - pop_len
        new_individuals = []
        while len(new_individuals) < lack:
            index_1, index_2 = sample(range(pop_len), 2)
            parent_1, parent_2 = self.population[index_1], self.population[index_2]
            child_1, child_2 = self.crossing_ox(parent_1.route, parent_2.route)
            new_individuals.append(TTPIndividual(child_1, self.ttp.init_knapsack()))
            new_individuals.append(TTPIndividual(child_2, self.ttp.init_knapsack()))
        new_individuals = new_individuals[:lack]
        self.population.extend(new_individuals)

    def selection_tour(self):
        shuffle(self.population)
        pop_len = len(self.population)
        lack = 0
        new_individuals = []
        while len(new_individuals) < lack:
            index_1, index_2 = sample(range(pop_len), 2)
            parent_1, parent_2 = self.population[index_1], self.population[index_2]
            child_1, child_2 = self.crossing_ox(parent_1.route, parent_2.route)
            new_individuals.append(TTPIndividual(child_1, self.ttp.init_knapsack()))
            new_individuals.append(TTPIndividual(child_2, self.ttp.init_knapsack()))
        new_individuals = new_individuals[:lack]
        self.population.extend(new_individuals)

    def mutation(self):
        for individual in self.population:
            if random() < self.pm:
                self.mutation_random_swap(individual.route)
                individual.fitness = None

    def mutation_gene(self):
        for individual in self.population:
            ran = sum(np.random.rand(self.ttp.cities_number) < self.pm)
            for _ in range(ran):
                self.mutation_random_swap(individual.route)
                individual.fitness = None

    def crossover(self, mode='random'):
        if mode == 'random':
            self.fix_population_random_ones()
        elif mode == 'winners':
            self.fix_population_only_winners()
        elif mode == 'ox_all':
            self.crossing_ox_all()
        elif mode == 'proper':
            self.crossing_properly()
        else:
            raise NameError

    def selection_tournament_only_winners(self):
        if self.tour == 0:
            return
        shuffle(self.population)
        winners = []
        tour_pop = []
        #self.print_population()
        while len(winners) < len(self.population):
            tour_pop = sample(self.population, self.tour)
            #self.print_population_fragment(tour_pop)
            winners.append(self.select_best_fitness_one(tour_pop))
            #print('best ', self.select_best_fitness_one(tour_pop).fitness)
            tour_pop.clear()
        self.population = winners

    def selection_tournament(self):
        shuffle(self.population)
        sliced = self.chunk_list(self.population, self.tour)
        winners = [self.select_best_fitness_one(tour_pop) for tour_pop in sliced]
        self.population = winners

    def selection_elit(self, percent):
        result = sorted(self.population, key=lambda k: k.fitness, reverse=True)
        number = int(self.pop_size * percent)
        winners = result[:number]
        self.population = winners

    def selection_roulette(self):
        winners = []
        for i in range(self.pop_size):
            winners.append(self.selection_roulette_pick())
        self.population = winners

    def selection_roulette_pick(self):
        max_ = sum(one.fitness / 1000. for one in self.population)
        pick = uniform(0, max_)
        current = 0
        for one in self.population:
            current += one.fitness / 1000.
            if current > pick:
                return one

    def print_population(self):
        for one in self.population:
            print(one.fitness, one.route, one.knapsack)

    def print_population_fragment(self, fragment):
        print('population fragment')
        for one in fragment:
            print(one.fitness, one.route, one.knapsack)
