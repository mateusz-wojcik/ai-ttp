from random import shuffle

class Knapsack:

    def __init__(self, max_weight):
        self.max_weight = max_weight
        self.weight = 0
        self.value = 0
        self.items = {}
        self.city_weights = []

    # items -> dict of dicts
    def take_best_item(self, possible_items):
        if not possible_items:
            return None
        return self.get_best_possible_item(possible_items)

    def take_random_item(self, possible_items):
        if not possible_items:
            return None
        return self.get_best_possible_item(possible_items)

    def get_best_possible_item(self, possible_items):
        for item in possible_items.items():
            if item[1]['WEIGHT'] <= self.get_weight_left():
                self.items[item[0]] = item[1]
                self.value += self.items[item[0]]['PROFIT']
                self.weight += self.items[item[0]]['WEIGHT']
                return item
        return None

    def get_possible_random_item(self, possible_items):
        shuffle(possible_items)
        for item in possible_items.items():
            if item[1]['WEIGHT'] <= self.get_weight_left():
                self.items[item[0]] = item[1]
                self.value += self.items[item[0]]['PROFIT']
                self.weight += self.items[item[0]]['WEIGHT']
                return item
        return None

    def get_value(self):
        val = 0
        for key, item in self.items.items():
            val += item['PROFIT']
        return val

    def get_weight(self):
        val = 0
        for key, item in self.items.items():
            val += item['WEIGHT']
        return val

    def get_weight_left(self):
        return self.max_weight - self.weight

    def empty(self):
        self.weight = 0
        self.value = 0
        self.items = {}
        self.city_weights = []

    def update_weights(self):
        self.city_weights.append(self.weight)
