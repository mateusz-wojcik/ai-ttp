import time

from GeneticAlgorithm import GeneticAlgorithm
from Loader import Loader
from TTP import TTP

POP_SIZE = 100
GEN = 100
PX = 0.7
PM = 0.01
TOUR = 5

print("start ", time.asctime(time.localtime(time.time())))
loader = Loader('trivial_0.ttp')
loader.load()
loader.parse()

ttp = TTP(loader.parameters)

ga = GeneticAlgorithm(POP_SIZE, GEN, PX, PM, TOUR, ttp)
best_solution = ga.main()
print(best_solution.route)
print(best_solution.knapsack.items)
print("weight", best_solution.knapsack.get_weight(), "max_weight", best_solution.knapsack.max_weight)
print("profit", best_solution.knapsack.get_value(), "profit", best_solution.knapsack.value)
print("best fitness", best_solution.fitness)
print("finish ", time.asctime(time.localtime(time.time())))
ttp.visualize(best_solution.route)

