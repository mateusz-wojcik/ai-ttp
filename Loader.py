import numpy as np


class Loader:
    BASE_PATH = "data/student/"

    def __init__(self, file_name):
        self.file_name = file_name
        self.file_content = None
        self.parameters = {}

    def load(self):
        with open(self.BASE_PATH + self.file_name) as file:
            self.file_content = file.read()
            print("Loaded from " + self.BASE_PATH + self.file_name)

    def parse(self):
        current_section = 'HEADER'
        for line in self.file_content.splitlines():
            line_splitted = line.split()

            if line_splitted[0] == 'NODE_COORD_SECTION':
                current_section = 'NODE_COORD_SECTION'
                self.parameters[current_section] = {}
                continue
            elif line_splitted[0] == 'ITEMS' and line_splitted[1] == 'SECTION':
                current_section = 'ITEMS_SECTION'
                self.parameters[current_section] = {}
                continue

            if current_section == 'HEADER':
                key, value = self.normalize_line(line_splitted)
                self.parameters[key] = value
            if current_section == 'NODE_COORD_SECTION':
                self.parameters[current_section][line_splitted[0]] = list(np.float_(line_splitted[1:]))
            if current_section == 'ITEMS_SECTION':
                self.parameters[current_section][line_splitted[0]] = ({
                                                         'PROFIT': self.float_try_parse(line_splitted[1])[0],
                                                         'WEIGHT': self.float_try_parse(line_splitted[2])[0],
                                                         'ASSIGNED_NODE_NUMBER': line_splitted[3]})

    def normalize_line(self, list_to_normalize):
        key, value = "", ""
        for item_index in range(len(list_to_normalize)):
            if list_to_normalize[item_index][-1] == ":":
                key = '_'.join(list_to_normalize[:item_index + 1])
                value = list_to_normalize[item_index + 1:]
        return key, self.float_try_parse(value[0])[0]

    @staticmethod
    def float_try_parse(value):
        try:
            return float(value), True
        except ValueError:
            return value, False