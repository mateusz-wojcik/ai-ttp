from random import shuffle, sample
import numpy as np
import matplotlib.pyplot as plt

from Knapsack import Knapsack


class TTP:
    def __init__(self, parameters):
        self.parameters = parameters
        self.result = self.assign_items_to_cities(self.parameters["ITEMS_SECTION"])
        self.v_min = self.parameters["MIN_SPEED:"]
        self.v_max = self.parameters["MAX_SPEED:"]
        self.capacity_of_knapsack = self.parameters["CAPACITY_OF_KNAPSACK:"]
        self.node_coord_section = self.parameters["NODE_COORD_SECTION"]
        self.cities_number = int(self.parameters["DIMENSION:"])
        self.distance_matrix = self.get_distance_matrix()
        self.criteria = 'PROFIT'
        self.sort_items_criteria()

    def shortest_route_tsp(self):
        cities_number = self.cities_number
        one_tsp = list(range(1, cities_number + 1))
        shuffle(one_tsp)
        start = one_tsp[0]
        route = []
        route.append(start)
        for i in range(cities_number - 1):
            route.append(self.take_shortest_possible(self.distance_matrix[len(route) - 1], route))
        return one_tsp

    def take_shortest_possible(self, cities_distance, route):
        index = -1
        best = np.inf
        for i in range(len(cities_distance)):
            if cities_distance[i] != 0 and cities_distance[i] < best and i not in route:
                index = i
                best = cities_distance[i]
        return index

    def sort_items_criteria(self):
        for key, item in self.result.items():
            dict_list_sorted = []
            if not item:
                pass
            if self.criteria == 'MIXED':
                dict_list_sorted = sorted(item.items(), key=lambda k: k[1]['PROFIT'] / k[1]['WEIGHT'])
            else:
                reverse = True if self.criteria == 'PROFIT' else False
                dict_list_sorted = sorted(item.items(), key=lambda k: k[1][self.criteria], reverse=reverse)
            tmp_dict = {}
            if dict_list_sorted is not []:
                for item in dict_list_sorted:
                    tmp_dict[item[0]] = item[1]
            self.result[key] = tmp_dict

    def time_summary_tsp(self, one_tsp, knapsack):
        result = .0
        for i in range(len(one_tsp)):
            # city_1 = self.node_coord_section[str(one_tsp[i])]
            # city_2 = self.node_coord_section[str(one_tsp[0 if i == len(one_tsp) - 1 else i + 1])]
            result += self.time_between_cities(one_tsp[i], one_tsp[0 if i == len(one_tsp) - 1 else i + 1],
                                               self.calculate_v(self.v_max, self.v_min, knapsack.city_weights[i],
                                                                self.capacity_of_knapsack))
        return result

    def greedy_algorithm_knp(self, one_tsp, knp_criteria, knapsack):
        knapsack.empty()
        for city in one_tsp:
            city_items = TTP.get_items_from_city(self, city)
            knapsack.take_best_item(city_items)
            knapsack.update_weights()
        return knapsack.items

    def greedy_algorithm_knp_random(self, one_tsp, knapsack):
        knapsack.empty()
        for city in one_tsp:
            city_items = TTP.get_items_from_city(self, city)
            knapsack.take_random_item(city_items)
            knapsack.update_weights()
        return knapsack.items

    def g_x_y(self, one_tsp, knapsack):
        self.greedy_algorithm_knp(one_tsp, self.criteria, knapsack)
        g_y = knapsack.value
        f_x_y = self.time_summary_tsp(one_tsp, knapsack)
        return g_y - f_x_y

    def g_x_y_random(self, one_tsp, knapsack):
        self.greedy_algorithm_knp_random(one_tsp, knapsack)
        g_y = knapsack.value
        f_x_y = self.time_summary_tsp(one_tsp, knapsack)
        return g_y - f_x_y

    def init_knapsack(self, capacity=None):
        return Knapsack(self.capacity_of_knapsack if capacity is None else capacity)

    def random_route_tsp(self, cities_number=None):
        if cities_number is None:
            cities_number = self.cities_number
        one_tsp = list(range(1, cities_number + 1))
        shuffle(one_tsp)
        return one_tsp

    def get_distance_matrix(self):
        cities_number = self.cities_number

        x = np.asarray([])
        y = np.asarray([])
        for i in range(cities_number):
            x = np.append(x, self.node_coord_section[str(i + 1)][0])
            y = np.append(y, self.node_coord_section[str(i + 1)][1])

        x1 = np.reshape(x, [cities_number, 1]).astype(float)
        y1 = np.reshape(y, [cities_number, 1]).astype(float)
        x2 = np.copy(x1).reshape([1, cities_number])
        y2 = np.copy(y1).reshape([1, cities_number])
        X = np.power(np.subtract(x1, x2), 2)
        Y = np.power(np.subtract(y1, y2), 2)
        distance_matrix = np.round(np.sqrt(X + Y), decimals=2)
        return distance_matrix

    def get_total_distance(self, route):
        total_dbc = 0
        for i in range(len(route) - 1):
            city_1 = self.node_coord_section[str(route[i])]
            city_2 = self.node_coord_section[str(route[0 if i == len(route) - 1 else i + 1])]
            total_dbc = total_dbc + self.distance_between_cities(city_1, city_2)

    def visualize(self, route, name=None):
        plt.clf()
        x = []
        y = []
        for i in range(len(route)):
            x_c = self.node_coord_section[str(route[i])][0]
            y_c = self.node_coord_section[str(route[i])][1]
            x.append(x_c)
            y.append(y_c)
            if i == 0:
                plt.plot(x_c, y_c, 'ro', label='Start')
            elif i == len(route) - 1:
                plt.plot(x_c, y_c, 'go', label='Koniec')
        # plt.plot(x, y, 'ro')
        plt.plot(x, y, linewidth=2)
        #plt.show()
        if name is not None:
            plt.title('Trasa złodzieja')
            plt.legend()
            plt.xlabel('X')
            plt.ylabel('Y')
            plt.savefig('plots/'+name+'.png')

    @staticmethod
    def visualize_population(bests, averages, weakest, name=None):
        plt.clf()
        x = list(range(1, len(bests) + 1))
        plt.plot(x, bests, linewidth=2, label='Najlepszy')
        plt.plot(x, averages, linewidth=2, label='Średni')
        plt.plot(x, weakest, linewidth=2, label='Najgorszy')
        #plt.show()
        if name is not None:
            plt.title('Przystosowanie osobników w populacji')
            plt.legend()
            plt.xlabel('Pokolenie')
            plt.ylabel('Fitness')
            plt.savefig('plots/'+name+'.png')

    @staticmethod
    def save_list_to_file(file_name, data_list):
        with open('filesdata/' + file_name, 'w') as f:
            for item in data_list:
                f.write("%s\n" % item)

    def distance_between_cities_index(self, city_1, city_2):
        return self.distance_matrix[city_1 - 1][city_2 - 1]

    @staticmethod
    def distance_between_cities(city_1, city_2):
        return np.sqrt(np.power(city_2[0] - city_1[0], 2) + np.power(city_2[1] - city_1[1], 2))

    def time_between_cities(self, city_1, city_2, v):
        return TTP.distance_between_cities_index(self, city_1, city_2) / v

    @staticmethod
    def calculate_v(v_max, v_min, w_curr, w_max):
        return v_max - (w_curr * ((v_max - v_min) / w_max))

    # items -> dict of city dicts
    @staticmethod
    def get_items_city(items, city_index):
        result = {}
        for key in items:
            if items[key]["ASSIGNED_NODE_NUMBER"] == str(city_index):
                result[key] = items[key]
        return result

    def get_items_from_city(self, city_index):
        result = self.result[str(city_index)]
        return result

    def assign_items_to_cities(self, items):
        result = {}
        for i in range(1, int(self.parameters["DIMENSION:"]) + 1):
            result[str(i)] = {}

        for key in items:
            result[items[key]["ASSIGNED_NODE_NUMBER"]][key] = items[key]
        return result
